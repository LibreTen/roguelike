extends Resource

class_name Enemy


export var xp := 0
export var strength := 1
export var delay :float = 1
var hp = 3
export var max_hp := 3

export var drops = ["green_blob"]

export var drop_rate = [0.30]

func _ready():
	hp = max_hp

func damage(final_attack_power):
	var final_defense_reduction = 0 # TODO
	var damage_taken = final_attack_power*(1-final_defense_reduction)
	hp -= damage_taken
	return hp <= 0 # is dead?

func attack():
	return strength
