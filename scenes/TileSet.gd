tool
extends TileSet

const FLOOR = 0
const WALL = 1

var binds = {
	FLOOR : [WALL],
	WALL : [FLOOR]
}


func _is_tile_bound(drawn_id, neighbor_id):
	if drawn_id in binds:
		return neighbor_id in binds[drawn_id]
	return false
