extends Node2D

class_name LevelBase

const enemy_base = preload("res://scenes/characters/Enemy.tscn")

onready var tile_map = $TileMap
onready var player = $Player

# Called when the node enters the scene tree for the first time.
func _ready():
	GM.initialize($Player,$TileMap,$CanvasLayer/UI)
	$Player.tile_map = tile_map



