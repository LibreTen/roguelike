extends Area2D

var tile_map : TileMap
var player


var stats : Enemy

onready var raycast = $RayCast2D

func _ready():
	stats = stats.duplicate()
	
#	tile_map = get_parent().tile_map
#	player = get_parent().player
	GM.add_participant(self)


func attacked(final_attack_power):
	if stats.damage(final_attack_power) == true:
		die()

func act():
	GM.act(self,stats.delay)
	var dir = tile_map.get_move_path(position,player.position)
	if dir.size() >= 2:
		raycast.cast_to = dir[1]-dir[0]
		raycast.force_raycast_update()
		
		if raycast.is_colliding():
			if raycast.get_collider().has_method("attacked") && !raycast.get_collider().is_in_group("enemy"):
				attack(raycast.get_collider())
		else:
			move_to(dir[1])
	GM.end_action()

func attack(target):
	target.attacked(stats.attack())

func move_to(pos):
	tile_map.free_tile(position)
	position = pos
	tile_map.occupy_tile(pos)
	force_update_transform()


func die():
	tile_map.free_tile(position)
	GM.remove_participant(self,true)
	$CollisionShape2D.free()
	force_update_transform()
	queue_free()
