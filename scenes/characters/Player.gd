extends Area2D

enum d {UP,DOWN,LEFT,RIGHT}

var tile_map : TileMap
onready var raycast = $RayCast2D

var turn = 0

var hp = 3

func _input(event):
	if event.is_action_pressed("up"):
		move_input(d.UP)
	if event.is_action_pressed("down"):
		move_input(d.DOWN)
	if event.is_action_pressed("left"):
		move_input(d.LEFT)
	if event.is_action_pressed("right"):
		move_input(d.RIGHT)
	if event.is_action_pressed("wait"):
		GM.act(self,1)
		GM.end_action()

func move_input(dir):
	var dirv
	match dir:
		d.UP:
			dirv = Vector2(0,-16)
		d.DOWN:
			dirv = Vector2(0,16)
		d.LEFT:
			dirv = Vector2(-16,0)
		d.RIGHT:
			dirv = Vector2(16,0)
	if try_move(dirv):
		move(dirv)

func try_move(dir):
	var can_move = tile_map.can_move_to(position+dir)
	#$RayCast2D.set_deferred("cast_to",dir)
	raycast.cast_to = dir
	raycast.force_raycast_update()
	#print($RayCast2D.is_colliding())
	if raycast.is_colliding():
		if raycast.get_collider().has_method("attacked"):
			attack(raycast.get_collider())
			
		return false
	return can_move


func move(dir):
	if GM.act(self,1):
		position += dir
		force_update_transform()
		GM.end_action()

func attack(target):
	if GM.act(self,1):
		target.attacked(GM.player.attack())
		GM.end_action()

func attacked(final_attack_power):
	GM.player.damage(final_attack_power)
