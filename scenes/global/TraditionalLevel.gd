extends Node

const LEVEL_SIZE = Vector2(30,30)
const ROOM_COUNT = 5
const MIN_ROOM_SIZE = 4
const MAX_ROOM_SIZE = 10

var tile_map : TileMap
var rooms = []
var map = []

var level_size

func build_level(t):
	tile_map = t
	tile_map.clear()
	rooms.clear()
	map.clear()
	
	level_size = LEVEL_SIZE
	for x in range(level_size.x):
		map.append([])
	
	var free_regions = [Rect2(Vector2(2,2),  level_size-Vector2(4,4))]
	var num_rooms = ROOM_COUNT
	for i in range(num_rooms):
		add_room(free_regions)
		if free_regions.empty():
			break

func add_room(free_regions):
	var region = free_regions[randi()%free_regions.size()]
	
	var size_x = MIN_ROOM_SIZE
	if region.size.x > MIN_ROOM_SIZE:
		size_x += randi() % int(region.size.x-MIN_ROOM_SIZE)
	
	var size_y = MIN_ROOM_SIZE
	if region.size.y > MIN_ROOM_SIZE:
		size_y += randi() % int(region.size.y-MIN_ROOM_SIZE)
	
	size_x = min(size_x,MAX_ROOM_SIZE)
	size_y = min(size_y,MAX_ROOM_SIZE)
	
	var start_x = region.position.x
	if region.size.x > size_x:
		start_x += randi() % int(region.size.x - size_x)
		
	var start_y = region.position.y
	if region.size.y > size_y:
		start_y += randi() % int(region.size.y - size_y)
	
	var room = Rect2(start_x, start_y, size_x, size_y)
	rooms.append(room)

	
	#cut_regions(free_regions, room)










