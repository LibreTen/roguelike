extends Node

var hp = 20
var max_hp = 20
var mp = 3
var max_mp = 3
var xp = 0
var level = 1


var strength = 1


var skill_points = 0

var inventory = []

var status_effects = []

func damage(final_attack_power):
	var final_defense_reduction = 0 # TODO
	var damage_taken = final_attack_power*(1-final_defense_reduction)
	hp -= damage_taken
	GM.ui.update()
	if hp <= 0:
		GM.die()

func attack():
	return strength

func die():
	hp = max_hp

func add_xp(v):
	xp += v
	if xp >= xp_to_next_level():
		level_up()
	GM.ui.update()

func level_up():
	xp -= xp_to_next_level()
	level += 1
	hp += ceil(max_hp*0.1)
	max_hp += ceil(max_hp*0.1)
	skill_points += 1
	strength += ceil(strength*0.1)
	

func xp_to_next_level():
	return ceil(pow(1.5,level))


func add_status_effect(effect_name : String, power : float):
	pass
