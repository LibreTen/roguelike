extends Node

const TILE_SIZE = 16

const LEVEL_SIZE = Vector2(30,30) #TEMP

var player_node : Node2D
onready var player := $PlayerManager
var tile_map : TileMap
var ui : Control

var current_floor = 0

var participants = []
const participant_base = {"node":null,"remaining_time":0}
const enemy_base = preload("res://scenes/characters/Enemy.tscn")


func _process(delta):
	pass
# Called when the node enters the scene tree for the first time.
func initialize(p,t,u):
	player_node = p
	tile_map = t
	ui = u
	var part = participant_base.duplicate()
	part.node = player_node
	part.remaining_time = 0
	participants.push_front(part)
	randomize()
	#$TraditionalLevel.build_level(t)


func build_level():
	tile_map.clear()
	
	var level_size = LEVEL_SIZE
	for x in range(level_size.x):
		
		for y in range(level_size.y):
			tile_map.set_cell(x,y,0)
			
	tile_map.update_bitmask_region(Vector2.ZERO,level_size)

func act(node,delay):
	for u in participants:
		if u.node == node:
			if u.remaining_time == 0:
				u.remaining_time += delay
				#update_time()
				return true
			
			break
	return false

func end_action():
	update_time()

func update_time():
	var min_time = null
	for u in participants:
		if min_time == null || u.remaining_time < min_time:
			min_time = u.remaining_time
	for u in participants:
		u.remaining_time -= min_time
	
	#print(participants[0].node.name)
	
	for u in participants:
		if u.remaining_time == 0:
			if u.node.has_method("act"):
				u.node.act()
				
			break

func add_participant(p):
	var part = participant_base.duplicate()
	part.node = p
	part.remaining_time = 0
	participants.append(part)



func remove_participant(p,killed):
	$PlayerManager.add_xp(p.stats.xp)
	for i in participants.size():
		if participants[i].node == p:
			participants.remove(i)
			break

func spawn_enemy(t,pos,parent):
	var type
	if t is String:
		type = load("res://resources/enemies/"+t+".tres")
	elif t is Enemy:
		type = t
	else: print("ENEMY TYPE IS WEIRD")
	
	var enemy = enemy_base.instance()
	enemy.stats = type
	enemy.position = pos*16
	enemy.player = player_node
	enemy.tile_map = tile_map
	parent.add_child(enemy)


func advance_floor():
	participants = []
	current_floor += 1
	get_tree().change_scene("res://scenes/levels/Level"+str(current_floor)+".tscn")

func die():
	participants = []
	player.die()
	current_floor = 0
	get_tree().change_scene("res://scenes/levels/Level0.tscn")
