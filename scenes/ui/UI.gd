extends Control


func _ready():
	update()

func update():
	$CharInfo/HPBar.max_value = GM.player.max_hp
	$CharInfo/HPBar.value = GM.player.hp
	
	$CharInfo/HPBar/Value.text = str(GM.player.hp)+" / "+str(GM.player.max_hp)
	$CharInfo/SP.text = "MP: "+str(GM.player.mp)+" / "+str(GM.player.max_mp)
	$CharInfo/XP.text = "XP: "+str(GM.player.xp)+" / "+str(GM.player.xp_to_next_level())
	$CharInfo/Level.text = "Lv: "+str(GM.player.level)
	
	
