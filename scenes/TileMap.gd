extends TileMap

var astar : AStar2D

const OCCUPY_WEIGHT = 4

# Called when the node enters the scene tree for the first time.
func _ready():
	astar = AStar2D.new()
	generate_grid()
	#print(astar.get_point_weight_scale(1))

func generate_grid():
	astar.clear()
	var size = get_used_rect().size
	astar.reserve_space(size.x*size.y)
	
	
	for x in size.x:
		for y in size.y:
			var tile_pos = Vector2(x,y)
			astar.add_point(get_tile_id(tile_pos),map_to_world(tile_pos))
	
	for x in size.x:
		for y in size.y:
			var id = get_tile_id(Vector2(x,y))
			var peek_tile_id
			
			
			peek_tile_id = get_tile_id(Vector2(x, y - 1))
			if astar.has_point(peek_tile_id):
				astar.connect_points(id, peek_tile_id, false)

			peek_tile_id = get_tile_id(Vector2(x, y + 1))
			if astar.has_point(peek_tile_id):
				astar.connect_points(id, peek_tile_id, false)
				
			peek_tile_id = get_tile_id(Vector2(x - 1, y))
			if astar.has_point(peek_tile_id):
				astar.connect_points(id, peek_tile_id, false)

			peek_tile_id = get_tile_id(Vector2(x + 1, y))
			if astar.has_point(peek_tile_id):
				astar.connect_points(id, peek_tile_id, false)
			
			if get_cell(x,y) != 0:
				astar.set_point_disabled(id,true)

func occupy_tile(pos):
	var id = get_tile_id(pos/16)
	astar.set_point_weight_scale(id,OCCUPY_WEIGHT)

func free_tile(pos):
	var id = get_tile_id(pos/16)
	astar.set_point_weight_scale(id,1)


func get_tile_id(tile_pos: Vector2) -> int:
	tile_pos = tile_pos - get_used_rect().position
	
	return int(tile_pos.x + (tile_pos.y * get_used_rect().size.x))


func get_move_path(current_pos: Vector2, target_pos: Vector2) -> Array:
	var start_id = get_tile_id(world_to_map(current_pos))
	var target_id = get_tile_id(world_to_map(target_pos))
	
	if astar.has_point(start_id) && astar.has_point(target_id):
		return Array(astar.get_point_path(start_id, target_id))
	return []

func can_move_to(pos: Vector2) -> bool:
	var mapPos = world_to_map(pos)
	var point_id = get_tile_id(mapPos)
	
	return astar.has_point(point_id) and ! astar.is_point_disabled(point_id)


